import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import 'package:path_provider/path_provider.dart';

Future main() async {
  runApp(CameraApp());
}

class CameraExample extends StatefulWidget {
  @override
  _CameraExampleState createState() {
    return _CameraExampleState();
  }
}

class _CameraExampleState extends State {
  Color filterColor = Colors.transparent;

  CameraController controller;
  List cameras;
  int selectedCameraIdx;
  String imagePath;

  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
     SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
  ]);
    availableCameras().then((availableCameras) {
      cameras = availableCameras;

      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });
      }
    }).catchError((err) {
      print('Error: $err.code\nError Message: $err.message');
    });
  }

  Future _takePicture() async {
    if (!controller.value.isInitialized) {
      // Fluttertoast.showToast(
      //     msg: 'Please wait',
      //     toastLength: Toast.LENGTH_SHORT,
      //     gravity: ToastGravity.CENTER,
      //     timeInSecForIos: 1,
      //     backgroundColor: Colors.grey,
      //     textColor: Colors.white
      // );

      return null;
    }

    // Do nothing if a capture is on progress
    if (controller.value.isTakingPicture) {
      return null;
    }

    final Directory appDirectory = await getApplicationDocumentsDirectory();
    final String pictureDirectory = '${appDirectory.path}/Pictures';
    await Directory(pictureDirectory).create(recursive: true);
    final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
    final String filePath = '$pictureDirectory/${currentTime}.jpg';

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    return filePath;
  }

  void _onCapturePressed() {
    _takePicture().then((filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
        });

        if (filePath != null) {
          // Fluttertoast.showToast(
          //     msg: 'Picture saved to $filePath',
          //     toastLength: Toast.LENGTH_SHORT,
          //     gravity: ToastGravity.CENTER,
          //     timeInSecForIos: 1,
          //     backgroundColor: Colors.grey,
          //     textColor: Colors.white
          // );
        }
      }
    });
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error: ${e.code}\nError Message: ${e.description}';
    print(errorText);

    // Fluttertoast.showToast(
    //     msg: 'Error: ${e.code}\n${e.description}',
    //     toastLength: Toast.LENGTH_SHORT,
    //     gravity: ToastGravity.CENTER,
    //     timeInSecForIos: 1,
    //     backgroundColor: Colors.red,
    //     textColor: Colors.white
    // );
  }

  @override
  Widget build(BuildContext context) {
    /// Display 'Loading' text when the camera is still loading.
    Widget _cameraPreviewWidget() {
      if (controller == null || !controller.value.isInitialized) {
        return const Text(
          'Loading',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.w900,
          ),
        );
      }

      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }

    /// Display the thumbnail of the captured image
    Widget _thumbnailWidget() {
      return Align(
        alignment: Alignment.centerLeft,
        child: imagePath == null
            ? SizedBox()
            : SizedBox(
                child: Image.file(File(imagePath)),
                width: 64.0,
                height: 64.0,
              ),
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 7,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: _cameraPreviewWidget(),
                    ),
                    Container(color: filterColor)
                  ],
                ),
              ),
              decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(
                  color: Colors.grey,
                  width: 3.0,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              color: Colors.black,
              child: Column(
                children: <Widget>[
                  Expanded(
                      flex: 5,
                      child: Container(
                        color: Colors.black,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: filterColor ==
                                                Colors.red.withOpacity(0.4)
                                            ? Colors.white
                                            : Colors.red,
                                        // borderRadius: BorderRadius.circular(5)
                                        shape: BoxShape.circle),
                                    padding: EdgeInsets.all(4),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        shape: CircleBorder(),
                                        primary: Colors.red,
                                      ),
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          filterColor =
                                              Colors.red.withOpacity(0.4);
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: filterColor ==
                                                Colors.green.withOpacity(0.4)
                                            ? Colors.white
                                            : Colors.green,
                                        // borderRadius: BorderRadius.circular(5)
                                        shape: BoxShape.circle),
                                    padding: EdgeInsets.all(4),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        shape: CircleBorder(),
                                        primary: Colors.green,
                                      ),
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          filterColor =
                                              Colors.green.withOpacity(0.4);
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Padding(
                                  padding: const EdgeInsets.all(15),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: filterColor ==
                                                Colors.blue.withOpacity(0.4)
                                            ? Colors.white
                                            : Colors.blue,
                                        // borderRadius: BorderRadius.circular(5)
                                        shape: BoxShape.circle),
                                    padding: EdgeInsets.all(4),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        shape: CircleBorder(),
                                        primary: Colors.blue,
                                      ),
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          filterColor =
                                              Colors.blue.withOpacity(0.4);
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                  Expanded(
                      flex: 5,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Stack(
                              children: <Widget>[
                                _thumbnailWidget(),
                                Align(
                                  alignment: Alignment.center,
                                  child: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          shape: CircleBorder(),
                                          primary: Colors.white),
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Container(
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                  color: Colors.black)),
                                        ),
                                      ),
                                      onPressed: () {
                                        _onCapturePressed();
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ))
                ],
              ),
            ),
          )
          // Padding(
          //   padding: const EdgeInsets.all(5.0),
          // ),
        ],
      ),
    );
  }
}

class CameraApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CameraExample(),
    );
  }
}
